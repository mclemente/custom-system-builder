const { getActorId } = require('../utils.js');
const { I } = inject();

/**********************************************/
/*            TEMPLATE STEPS                  */
/**********************************************/

When(/^I type '(.*)' as text field label$/, (contents) => {
    I.fillField('#textFieldLabel', contents);
});

When(/^I type '(.*)' as text field allowed character list$/, (contents) => {
    I.fillField('#textFieldCharList', contents);
});

When(/^I type '(.*)' as text field maximum length$/, (contents) => {
    I.fillField('#textFieldMaxLength', contents);
});

When(/^I type '(.*)' as text field default value$/, (contents) => {
    I.fillField('#textFieldValue', contents);
});

/**********************************************/
/*            CHARACTER STEPS                 */
/**********************************************/
